import pytest

from src.utils import Filter


class TestCase:
    def __init__(self, number, expected_value):
        self.number = number
        self.expected_value = expected_value


base_test_cases = [
    TestCase(number="1.25", expected_value=True),
    TestCase(number=" ", expected_value=False),
    TestCase(number="any", expected_value=False),
]


class TestNumberFilter:
    @pytest.mark.parametrize("test_case", base_test_cases)
    def test_base_cases(self, test_case):

        result = Filter.number_filter(test_case.number)

        assert test_case.expected_value == result
