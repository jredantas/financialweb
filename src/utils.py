from flask import Blueprint

account_api = Blueprint("account_api", __name__)


class Formatter:
    @staticmethod
    def format_date(a_date: str) -> str:
        a_date = a_date.split("/")
        formatted_date = str(a_date[-1] + "-" + a_date[-2] + "-" + a_date[0])
        return formatted_date


class Filter:
    @account_api.app_template_filter("number")
    def number_filter(s):
        try:
            float(s)
            return True
        except Exception as err:
            err
            return False
